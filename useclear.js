const clear  = require('clear');

console.log('Printing first line to the console.');
console.log('Then the second line,');
console.log(' and then the third.');
console.log('Now clearing the console after three seconds.');
setTimeout(function() {
    clear();
}, 3000); 
