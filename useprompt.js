const prompt = require('prompt');

prompt.start();

const schema = {
    properties: {
        topic: {
            description: 'Which topic you want to learn today?',
            pattern: /^[a-zA-Z\.\s\-]+$/,
            message: 'Topic must be only letters, spaces,dots, or dashes',
            required: true
        },
        password: {
            description: 'Enter your password.',
            hidden: true,
            replace: '*',                        // If `hidden` is set it will replace each hidden character with the specified string.
            default: 'badpassword',             // Default value to use if no value is entered.
            required: true 
        }
    }
};


prompt.get(schema, function (err, result) {
    console.log('=============Your Inputs=============');
    console.log('  You have chosen ' + result.topic + ' to learn.');
    console.log('  password: ' + result.password);
});
