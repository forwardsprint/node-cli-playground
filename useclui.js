const clui = require('clui'),
    clc = require('cli-color');
const os = require('os');
const Sparkline = require('clui').Sparkline;

const printTable = () => {
    var Line = clui.Line,
        LineBuffer = clui.LineBuffer;

    var outputBuffer = new LineBuffer({
        x: 0,
        y: 0,
        width: 'console',
        height: 'console'
    });

    var message = new Line(outputBuffer)
        .column('Test Scores', 20, [clc.green])
        .fill()
        .store();

    var blankLine = new Line(outputBuffer)
        .fill()
        .store();

    var header = new Line(outputBuffer)
        .column('Students', 15, [clc.cyan])
        .column('Test 1', 15, [clc.cyan])
        .column('Test 2', 15, [clc.cyan])
        .column('Test 3', 15, [clc.cyan])
        .column('Test 4', 11, [clc.cyan])
        .fill()
        .store();

    var line;
    for (var l = 0; l < 20; l++) {
        line = new Line(outputBuffer)
            .column('Student '.concat(+l + 1), 15)
            .column((Math.random() * 100).toFixed(3), 15)
            .column((Math.random() * 100).toFixed(3), 15)
            .column((Math.random() * 100).toFixed(3), 15)
            .column((Math.random() * 100).toFixed(3), 11)
            .fill()
            .store();
    }

    outputBuffer.output();

};

const printGauge = () => {

    var Gauge = clui.Gauge;

    var total = os.totalmem();
    var free = os.freemem();
    var used = total - free;
    var mbytes = Math.ceil(used / 1000000) + ' MB';
    var total_mbytes = Math.ceil(total / 1000000) + ' MB';
    console.log(Gauge(used, total, 20, total * 0.8, mbytes));
    console.log('Used ' + mbytes + ' of ' +  total_mbytes);
}

const printSparkLine = () => {
    var studentsPerHour = [10,12,3,7,12,9,23,10,9,19,16,18,12,12];
    console.log(Sparkline(studentsPerHour, ' Students/Hour'));
}
console.log('==================Printing Table of Test Scores==================');
printTable();
console.log('==================Printing Memory Used==================');
printGauge();
console.log('==================Printing Test Taking Students Per Hour==================');
printSparkLine();