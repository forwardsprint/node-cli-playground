const parseArgs = require('minimist')

const argv = parseArgs(process.argv)

console.log('Run the program and provide following options.');
console.log('-t OR --topic <space/=> <your topic name>');
console.log('-p OR --time <space/=> <time you are going to spend on the topic>');
console.log('-v OR --video <space/=> <true if you are going to view videos, false if you are not.>');
console.log('-x OR --text <space/=> <true if you are going to read, false if you are not.>');

if (argv.title || argv.t) {
    var topic = argv.title ? argv.title : argv.t;
    console.log(`You are going to learn ${topic} today.`);
}
if (argv.time || argv.p) {
    var timePeriod = argv.time ? argv.time : argv.p;
    console.log(`You are going to spend ${timePeriod} for learning your today's topic.`);
}

if (argv.video || argv.v) {
    console.log(`You will use video content.`);
}

if (argv.text || argv.x) {
    console.log(`You will use textual content.`);
}