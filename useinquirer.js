const inquirer = require('inquirer');

const questions = [
    {
      type: 'input',
      name: 'topicToLearn',
      message: 'What topic do you want to learn today?',
      default: 'Node.js'
    },
    {
      type: 'list',
      name: 'medium',
      message: 'What is your preferred medium?',
      choices: ['Video', 'Text', 'Exercises'],
      filter: function(val) {
        return val.toLowerCase();
      }
    },
    {
      type: 'expand',
      name: 'timeOfDay',
      message: 'What is your preferred time to receive the lessons(Morning, Afternoon, Evenings)?',
      choices: [
        {
          key: 'm',
          name: 'Morning',
          value: 'morning'
        },
        {
          key: 'a',
          name: 'Afternoon',
          value: 'afternoon'
        },
        {
          key: 'e',
          name: 'Evenings',
          value: 'evenings'
        }
      ]
    },
    {
      type: 'rawlist',
      name: 'delivery',
      message: 'How would you access the learning material?',
      choices: ['Email', 'Our Website', 'Slack', 'Telegram', 'WhatsApp']
    },
    {
      type: 'input',
      name: 'email',
      message: "What's your email?",
      validate: function(value) {
        var pass = value.match(
          /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/i
        );
        if (pass) {
          return true;
        }
  
        return 'Please enter a valid email address';
      }
    },
    {
      type: 'input',
      name: 'comments',
      message: 'Any additional comments?',
      default: 'Nope, all good!'
    },
    {
        type: 'checkbox',
        message: 'Would you refer our services to your freinds, colleagures, or business associates?',
        name: 'referral',
        choices: [
          {
            name: 'Yes'
          },
          {
            name: 'No'
          },
          {
            name: 'Can\'t Say'
          }
        ]
    }, {
      type: 'list',
      name: 'prize',
      message: 'If you provide a referral, you get a week\'s access to our training content for free',
      choices: ['Sure, I would love the access against a referral.', 'No thanks! I don\'t want the free access.'],
      when: function(answers) {
        return answers.referral !== 'Yes' ;
      }
    }
  ];

inquirer
  .prompt(questions)
  .then(answers => {
    console.log("============Your Answers============");
    console.log(JSON.stringify(answers, null, '  '));
  })
  .catch(error => {
    if(error.isTtyError) {
      // Prompt couldn't be rendered in the current environment
    } else {
      // Something else when wrong
    }
  });