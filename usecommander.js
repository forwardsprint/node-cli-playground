const program = require('commander')
program.version("0.0.1")

program
  .option('-t, --learning-topic <topic>', 'The topic that you want to learn today')
  .option('-p, --learning-period <period>', 'The time that you will spend learning the topic')
  .option('-v, --video', 'Learn from online video')
  .option('-t, --text', 'Learn using text material');

program.parse(process.argv);

console.log(`Your today's learning plan:`);

if (program.learningTopic) {
	console.log(`You are going to learn ${program.learningTopic} today.`);
} else {
    console.log(`You have not chosen any learning topic.`);
}

if (program.learningPeriod) {
	console.log(`You are going to spend ${program.learningPeriod} for learning your today's topic.`);
} else {
    console.log(`You have not set aside any time for your learning. Planning is the first step to success.`);
}

if (program.video) {
	console.log(`You will use video lessons.`);
} 

if (program.text) {
	console.log(`You will learn this topic by reading.`);
}


