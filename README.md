# node-cli-playground

The project contains sample code for modules useful for developing Node.js CLI applications. It implements following modules;

1. Commander.js
2. Yargs.js
3. minimist.js
4. Inquirer.js
5. Prompt.js
6. Clear.js
7. Clui.js

# How to run samples?

Clone this repository. Install all npm packages.

npm i

Then, on the terminal (or command prompt), use "node <filename> <options>" commands to see the output. 
