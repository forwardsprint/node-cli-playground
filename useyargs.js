const yargs = require('yargs')

yargs.command ({
	command: 'topic',
	description: 'The topic that you want to learn today',
	builder: {
		title: {
			description: 'The topic title',
			demandOption: true,
			type: 'string'
        },
        time: {
			description: 'The time that you will spend learning the topic',
			demandOption: true,
			type: 'string'
        },
        video: {
			description: 'Whether you will use video lessons.',
			demandOption: true,
			type: 'boolean'
        },
        text: {
			description: 'Whether you will use textual content.',
			demandOption: true,
			type: 'boolean'
        }
	},
	handler: function (argv) {
        console.log(`You are going to learn ${argv.title} today.`);
        console.log(`You are going to spend ${argv.time} for learning your today's topic.`);
        if (argv.video) {
            console.log(`You will use video content.`);
        }
        if (argv.text) {
            console.log(`You will use textual content.`);
        }
        
	} 
});

yargs.parse();

